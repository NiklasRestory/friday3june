﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using XUnitTestingTestverktygLektion7;

namespace XUnitTestingTestverktygLektion7_Test
{
    [Collection("Name")]
    public class NameTest
    {
        private NameFixture nameFixture;

        public NameTest(NameFixture nameFixtureParameter)
        {
            nameFixture = nameFixtureParameter;
        }

        [Fact]
        [Trait("Name", "FullName")]
        public void GetFullName_Joe_Lowell()
        {
            Name name = nameFixture.name;
            name.firstName = "Joe";
            name.lastName = "Lowell";
            string expected = "Joe Lowell";

            string actual = name.GetFullName();

            Assert.Equal(expected, actual, ignoreCase:true);
            Assert.Contains("oe", actual, StringComparison.InvariantCultureIgnoreCase);
            Assert.StartsWith("jo", actual, StringComparison.InvariantCultureIgnoreCase);
            Assert.EndsWith("ell", actual, StringComparison.InvariantCultureIgnoreCase);
            Assert.Matches("[A-Z]{1}[a-z]+ [A-Z]{1}[a-z]+", actual);

            //Cleanup
            name.firstName = null;
            name.lastName = null;
        }

        [Fact]
        [Trait("Name", "FullName")]
        public void Name_ShouldBeNull_IfWeHaventAddedAny()
        {
            Name name = nameFixture.name;
            name.setFirstName("");
            Assert.Null(name.firstName);
        }

        [Fact]
        [Trait("Name", "FullName")]
        public void Name_ShouldNotBeNull_IfWeAdded()
        {
            Name name = nameFixture.name;
            name.setFirstName("George");
            Assert.NotNull(name.firstName);

            name.firstName = null;
        }

        [Fact]
        [Trait("Name", "FullName")]
        public void setFirstNameThrowsError_WhenEmptyString()
        {
            Name name = nameFixture.name;
            Assert.Throws<ArgumentException>(
                () => name.setFirstNameThrowsError("")
            );
        }

       
    }
}
